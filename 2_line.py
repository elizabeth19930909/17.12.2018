# coding=utf-8
import itertools
#line = 'abc'
line = raw_input('Print the line ')

# решение одной строкой
lines = [''.join(x) for x in itertools.permutations(line)]
print lines

# мое решение, более длинное
combinations = []
for x in itertools.permutations(line, len(line)):
    el = ''.join(x)
    combinations.append(el)
print combinations
# если нужна автоматическая нумерация в цикле можно использовать enumerate
