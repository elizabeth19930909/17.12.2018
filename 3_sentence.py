# coding=utf-8
import itertools
text = 'yes and no'
word = text.split()

# решение одной строкой
result = [' '.join(x) for x in itertools.permutations(word)]
print result

# мое решение, более длинное
result = []
for x in itertools.permutations(word, len(word)):
    el = ' '.join(x)
    result.append(el)
print result
