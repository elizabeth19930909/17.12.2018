# 1
L = [1, 2, 3, 4]
A = list(reversed(L))
print '1 task: List', L, 'Reversed list', A

# 2

M = ['one', 'two', 'oneandtwo', 'three', 'none', 'four']
M1 = [x for x in M if x[:3] == 'one']
M2 = [x for x in M if x[:3] != 'one']
print '2 task:', M, M1, M2

# 3
N = [x for x in range(0, 10, 2)]
print '3 task: Even numbers list', N

# 4
K = [x for x in range(0, 15) if x%3 == 0]
print '4 task:', K

# 5
P = [x*10 for x in range(0, 10) if x%2 == 0]
print '5 task: Even numbers list*10', P