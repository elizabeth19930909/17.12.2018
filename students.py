alexander = [4, 4, 3, 3]
mihail = [4, 4, 3, 3]
elena = [4, 4, 5, 5]
svetlana = [5, 5, 5, 5]

D = {'Alexander': alexander, 'Mihail': mihail, 'Elena': elena, 'Svetlana': svetlana}
subjects = {0:'algebra', 1:'geometry', 2:'russian language', 3:'literature'}

# average point for student
names = D.keys()
B = {}
for i in range(len(D)):
    sum = 0
    for j in range (len(D[names[i]])):
        sum = sum + D[names[i]][j]
    a_point = sum/float(len(D[names[i]]))

    print 'Average point for student %s = %s' % (names[i], a_point)

    B.setdefault(a_point, [])
    B[a_point].append(names[i])

best = max(B.keys())
worst = min(B.keys())
print 'Best score %s %s, worst score %s %s' % (B.get(best), best, B.get(worst), worst)

# average point for subjects

for i in range(len(subjects)):
    sum=0
    for j in range(len(names)):
        sum= sum + D[names[j]][i]
    a_point = sum / float(len(names))
    print 'Average point for subject %s = %s' % (subjects[i], a_point)
