# coding=utf-8
mydict = {'firstkey': 'firstvalue', 'secondkey': 'secondvalue'}

key = raw_input('Print the key: ')

# 1 Используя .get можно получить не ошибку KeyError, а None
print mydict.get(key)

# 2 Вывести на печать при несуществующем ключе строку ‘the key is not in the dictionary’
if key in mydict:
   print mydict.get(key)
else:
   print'The key is not in the dictionary'

# решение одной строкой - final version
print mydict.get(key, 'The key is not in the dictionary')
